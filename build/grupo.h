#pragma once
#ifndef Grupo_H
#define Grupo_H
#include <vector>
#include "figura.h"
#include "transformacoes.h"

typedef struct grupo {

	std::vector<figura> conjunto_figuras;
	std::vector<transformacao> conjunto_transformacoes;
}Grupo;

#endif 

