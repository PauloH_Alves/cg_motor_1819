
#ifndef Animacao_H
#define Animacao_H
#include <vector>

typedef struct verticeANIM {
	float x;
	float y;
	float z;
};

typedef struct animacao {
	float tempo_animacao=0;
	//inicial da transformacao
	char tipo_animacao;
	//caso nada mude as animacoes sao sempre repetidas quando chegam ao final
	bool infinita = true;
	std::vector<verticeANIM> pontos_animacao;
};



#endif
