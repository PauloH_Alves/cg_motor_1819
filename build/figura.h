#pragma once
#ifndef Figura_H
#define Figura_H
#include <vector>
#include "transformacoes.h"
#include "animacao.h"


typedef struct vertice {
	float x;
	float y;
	float z;
};

typedef struct figura {
	std::vector<transformacao> transformacoes;
	vertice *conjunto_vertices;
	int n_vertices;

	std::vector<animacao> animacoes;
};




#endif
