#include <stdlib.h>
#include <iostream>
#ifdef __APPLE__
#include <GLUT/glut.h>
#else
#include <GL/glut.h>
#endif

#define _USE_MATH_DEFINES
#include <math.h>
#include <fstream>
#include <string>
#include <sstream>
#include <algorithm>
#include "build/tinyxml2.h"
#include "build/cena.h"
#include "build/figura.h"
#include "build/grupo.h"
#include "build/transformacoes.h"
#include "build/animacao.h"


using namespace tinyxml2;

void desenhar_Esfera(figura f);
void desenhar_figura(figura f);
void desenhar_grupo(grupo g);
void desenhar_cena(cena c);
void timer(int a);
void timer2(int a);
void timer3(int a);



// Variaveis para calcular e guardar FPS
int times, timebase, frame = 0, fps = 0;
char tituloJanela[64] = "";

//variaveis para camara

float alfa = 0.0f, beta = 0.0f, radius = 5.0f;
float camX=0.0, camY=0.0, camZ=15.0;
float e1_x = 0, e1_y = 0, e1_z = 0;
float e2_x = 0, e2_y = 0, e2_z = 0;
float e3_x = 0, e3_y = 0, e3_z = 0;



//mouse
int startX, startY, tracking = 0;
//


void spherical2Cartesian() {
	camX = radius * cos(beta) * sin(alfa);
	camY = radius * sin(beta);
	camZ = radius * cos(beta) * cos(alfa);
}


cena conjunto_cenas;

XMLDocument cena_temp;
verticeANIM ponto_catmull(verticeANIM p0, verticeANIM p1, verticeANIM p2, verticeANIM p3,float t) {
	verticeANIM temp;
		float t2 = t * t;
		float t3 = t * t * t;
		
		temp.x = 0;
		temp.y = 0;
		temp.z = 0;
		temp.x = 0.5 *((2 * p1.x) +
			(-p0.x + p2.x) * t +
			(2 * p0.x - 5 * p1.x + 4 * p2.x - p3.x) * t2 +
			(-p0.x + 3 * p1.x - 3 * p2.x + p3.x) * t3);

		temp.y = 0.5 *((2 * p1.y) +
			(-p0.y + p2.y) * t +
			(2 * p0.y - 5 * p1.y + 4 * p2.y - p3.y) * t2 +
			(-p0.y + 3 * p1.y - 3 * p2.y + p3.y) * t3);

		temp.z = 0.5 *((2 * p1.z) +
			(-p0.z + p2.z) * t +
			(2 * p0.z - 5 * p1.z + 4 * p2.z - p3.z) * t2 +
			(-p0.z + 3 * p1.z - 3 * p2.z + p3.z) * t3);

	return temp;

}verticeANIM getsplinepoint(float t, std::vector<verticeANIM> pontos_controlo) {
	verticeANIM ponto_calculado;
	int p0, p1, p2, p3;
	p1 = (int)t + 1;
	p2 = p1 + 1;
	p3 = p2 + 1;
	p0 = p1 - 1;

	t = t - (int)t;
	float tt = t * t;
	float ttt = tt * t;

	float q1 = -ttt + 2.0f * tt -t;
	float q2 = 3.0f * ttt -5.0f * tt + 2.0f;
	float q3 = -3.0f * ttt +4.0f * tt +t;
	float q4 = ttt - tt;

	ponto_calculado.x = 0.5f * (pontos_controlo.at(p0).x * q1 + pontos_controlo.at(p1).x * q2 + pontos_controlo.at(p2).x * q3 + pontos_controlo.at(p3).x * q4);
	ponto_calculado.y = 0.5f * (pontos_controlo.at(p0).y * q1 + pontos_controlo.at(p1).y * q2 + pontos_controlo.at(p2).y * q3 + pontos_controlo.at(p3).y * q4);
	ponto_calculado.z = 0.5f * (pontos_controlo.at(p0).z * q1 + pontos_controlo.at(p1).z * q2 + pontos_controlo.at(p2).z * q3 + pontos_controlo.at(p3).z * q4);
	return ponto_calculado;
}
std::vector<verticeANIM> pontos_catmull(std::vector<verticeANIM> conj_pontos,int divisoes) {
	std::vector<verticeANIM> vetor_calculado;
	verticeANIM vtemp;
		for (float t = 0.0f; t < (float)conj_pontos.size() -3; t += 0.05f) {
			//vtemp = ponto_catmull(conj_pontos.at(i), conj_pontos.at(i + 1), conj_pontos.at(i + 2), conj_pontos.at(i + 3), t);
			vtemp = getsplinepoint(t, conj_pontos);
			vetor_calculado.push_back(vtemp);
		}

	return vetor_calculado;
}





// teste catmull

figura catmull;

// teste catmull



void changeSize(int w, int h) {

	// Prevent a divide by zero, when window is too short
	// (you cant make a window with zero width).
	if(h == 0)
		h = 1;

	// compute window's aspect ratio 
	float ratio = w * 1.0 / h;

	// Set the projection matrix as current
	glMatrixMode(GL_PROJECTION);
	// Load Identity Matrix
	glLoadIdentity();
	
	// Set the viewport to be the entire window
    glViewport(0, 0, w, h);

	// Set perspective
	gluPerspective(45.0f ,ratio, 1.0f ,1000.0f);

	// return to the model view matrix mode
	glMatrixMode(GL_MODELVIEW);
}



void renderScene(void) {
	
	// clear buffers
	
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	// set the camera
	glLoadIdentity();

	gluLookAt(camX, camY, camZ,
		0.0, 0.0, 0.0,
		0.0f, 1.0f, 0.0f);


	// put drawing instructions here
	glColor3b(0, 60, 0);
    glPointSize(2);             //and this one


	//teste
	//glColor3b(50, 50, 50);
	
	//glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
	glLineWidth(2);
	glBegin(GL_LINES);
		
		glVertex3f(0,0,0); 
		glVertex3f(50, 0, 0);
		glVertex3f(0, 0, 0);
		glVertex3f(0, 50, 0);
		glVertex3f(0, 0, 0);
		glVertex3f(0, 0, 50);
	glEnd();

	glColor3b(60, 60, 60);

	for (int i = 0; i < conjunto_cenas.grupos.at(0).conjunto_figuras.at(0).animacoes.at(0).pontos_animacao.size(); i++) {
		glBegin(GL_POINTS);
		glVertex3f(conjunto_cenas.grupos.at(0).conjunto_figuras.at(0).animacoes.at(0).pontos_animacao.at(i).x,
			conjunto_cenas.grupos.at(0).conjunto_figuras.at(0).animacoes.at(0).pontos_animacao.at(i).y,
			conjunto_cenas.grupos.at(0).conjunto_figuras.at(0).animacoes.at(0).pontos_animacao.at(i).z);
		glEnd();
	}
	for (int i = 0; i < catmull.n_vertices-1; i++) {
		glBegin(GL_LINES);

		glVertex3f(catmull.conjunto_vertices[i].x, catmull.conjunto_vertices[i].y, catmull.conjunto_vertices[i].z);
		i++;
		glVertex3f(catmull.conjunto_vertices[i].x, catmull.conjunto_vertices[i].y, catmull.conjunto_vertices[i].z);
		i--;
		glEnd();

	}
	glPushMatrix();
	glScalef(3, 0, 3);
	for (int i = 0; i < catmull.n_vertices - 1; i++) {
		glBegin(GL_LINES);

		glVertex3f(catmull.conjunto_vertices[i].x, catmull.conjunto_vertices[i].y, catmull.conjunto_vertices[i].z);
		i++;
		glVertex3f(catmull.conjunto_vertices[i].x, catmull.conjunto_vertices[i].y, catmull.conjunto_vertices[i].z);
		i--;
		glEnd();

	}
	glPopMatrix();
	glPushMatrix();
	glScalef(6, 0, 6);
	for (int i = 0; i < catmull.n_vertices - 1; i++) {
		glBegin(GL_LINES);

		glVertex3f(catmull.conjunto_vertices[i].x, catmull.conjunto_vertices[i].y, catmull.conjunto_vertices[i].z);
		i++;
		glVertex3f(catmull.conjunto_vertices[i].x, catmull.conjunto_vertices[i].y, catmull.conjunto_vertices[i].z);
		i--;
		glEnd();

	}
	glPopMatrix();
	/*glColor3b(40, 40, 0);
	glutSolidSphere(3, 20, 20);
	glColor3b(60, 60, 60);*/
	glPushMatrix();
	glTranslatef(e1_x, e1_y, e1_z);
	glutSolidSphere(1,20,20);
	glPopMatrix();
	/*
	glPushMatrix();
	glTranslatef(e2_x, e2_y, e2_z);
	glutSolidSphere(1, 20, 20);
	glPopMatrix();
	glPushMatrix();
	glTranslatef(e3_x, e3_y, e3_z);
	glutSolidSphere(2, 20, 20);
	glPopMatrix();*/
	
	//desenhar_cena(conjunto_cenas);

	//fps e titulo da janela, escrito por cima do glutcreatewindow(titulo) do main
	frame++;
	times = glutGet(GLUT_ELAPSED_TIME);
	if (times - timebase > 1000) {
		fps = frame * 1000.0 / (times - timebase);
		timebase = times;
		frame = 0;
	}
	sprintf(tituloJanela, "Motor_Grafico_CG_2�_Fase      FPS  %d", fps);
	glutSetWindowTitle(tituloJanela);

	// End of frame
	glutSwapBuffers();
}

void load_catmull(std::string ficheiro) {

	//abre ficheiro para leitura
	std::ifstream stream_leitura;
	stream_leitura.open(ficheiro);

	//linha de texto e quantidade de vertices e outros auxiliares para leitura
	std::string xString, yString, zString, line;
	float vertice_x, vertice_y, vertice_z;
	int i = 0, n_vertices;

	//ler numero de vertices e converter para inteiro
	if (getline(stream_leitura, line)) {
		n_vertices = stoi(line);
	}

	//allocar conjunto de vertices e guardar o total de vertices na figura, poderia mudar para realloc
	catmull.conjunto_vertices = (vertice*)malloc(n_vertices * sizeof(vertice));
	catmull.n_vertices = n_vertices;
	//ler vertices
	while (n_vertices > 0) {
		if (!getline(stream_leitura, line)) break;
		std::stringstream ss(line);
		getline(ss, xString, ',');
		getline(ss, yString, ',');
		getline(ss, zString, ',');

		vertice_x = std::stof(xString);
		vertice_y = std::stof(yString);
		vertice_z = std::stof(zString);
		catmull.conjunto_vertices[i].x = vertice_x;
		catmull.conjunto_vertices[i].y = vertice_y;
		catmull.conjunto_vertices[i].z = vertice_z;

		i++;
		n_vertices--;
	}


}



void desenhar_Esfera(figura f) {


	//glColor3b(1, 10, 80);
	//glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
	glBegin(GL_TRIANGLES);
	for (int i = 1; i < 25 + 1; i++) {
		glVertex3f(f.conjunto_vertices[0].x, f.conjunto_vertices[0].y, f.conjunto_vertices[0].z);
		glVertex3f(f.conjunto_vertices[i].x, f.conjunto_vertices[i].y, f.conjunto_vertices[i].z);
		glVertex3f(f.conjunto_vertices[i + 1].x, f.conjunto_vertices[i + 1].y, f.conjunto_vertices[i + 1].z);


	}
	glEnd();

	glBegin(GL_TRIANGLES);
	for (int i = 1; i < f.n_vertices - 25 - 1; i++) {
		//glColor3b(10+i/400, 10 + i / 400, 10 + i / 400);
		glVertex3f(f.conjunto_vertices[i].x, f.conjunto_vertices[i].y, f.conjunto_vertices[i].z);
		glVertex3f(f.conjunto_vertices[i + 25 + 1].x, f.conjunto_vertices[i + 25 + 1].y, f.conjunto_vertices[i + 25 + 1].z);
		glVertex3f(f.conjunto_vertices[i + 1].x, f.conjunto_vertices[i + 1].y, f.conjunto_vertices[i + 1].z);
		glVertex3f(f.conjunto_vertices[i + 1].x, f.conjunto_vertices[i + 1].y, f.conjunto_vertices[i + 1].z);
		glVertex3f(f.conjunto_vertices[i + 25 + 2].x, f.conjunto_vertices[i + 25 + 2].y, f.conjunto_vertices[i + 25 + 2].z);
		glVertex3f(f.conjunto_vertices[i + 25 + 1].x, f.conjunto_vertices[i + 25 + 1].y, f.conjunto_vertices[i + 25 + 1].z);


	}
	glEnd();

}
void desenhar_figura(figura f) {
	
	std::size_t i = 0;
	//guardar a matriz sem transformacao
	glPushMatrix();
	//glColor3b(i+10, i+10, i+10);
	while (i < f.transformacoes.size()) {
			
		if (f.transformacoes[i].tipo == 't')
			glTranslatef(f.transformacoes[i].x, f.transformacoes[i].y, f.transformacoes[i].z);
		if (f.transformacoes[i].tipo == 'r')
			glRotatef(f.transformacoes[i].angulo, f.transformacoes[i].x, f.transformacoes[i].y, f.transformacoes[i].z);
		if (f.transformacoes[i].tipo == 'e')
			glScalef(f.transformacoes[i].x, f.transformacoes[i].y, f.transformacoes[i].z);
			
		i++;
	}

	desenhar_Esfera(f);
	glPopMatrix();

}

void desenhar_grupo(grupo g) {

	for (figura f : g.conjunto_figuras) {
		desenhar_figura(f);
	}
}


void desenhar_cena(cena c) {

	for (grupo g : c.grupos) {
		desenhar_grupo(g);
	}


}

void ler_figura(std::string ficheiro, figura nova_figura,std::vector<transformacao>conjunto_t,std::vector<animacao> conjunto_a,grupo* g){

	//abre ficheiro para leitura
	std::ifstream stream_leitura;
	stream_leitura.open(ficheiro);

	//linha de texto e quantidade de vertices e outros auxiliares para leitura
	std::string xString, yString, zString, line;
	float vertice_x, vertice_y, vertice_z;
	int i = 0, n_vertices;

	//ler numero de vertices e converter para inteiro
	if (getline(stream_leitura, line)) {
		n_vertices = stoi(line);
	}

	//allocar conjunto de vertices e guardar o total de vertices na figura, poderia mudar para realloc
	nova_figura.conjunto_vertices = (vertice*)malloc(n_vertices * sizeof(vertice));
	nova_figura.n_vertices = n_vertices;
	//ler vertices
	while (n_vertices > 0) {
		if (!getline(stream_leitura, line)) break;
		std::stringstream ss(line);
		getline(ss, xString, ',');
		getline(ss, yString, ',');
		getline(ss, zString, ',');

		vertice_x = std::stof(xString);
		vertice_y = std::stof(yString);
		vertice_z = std::stof(zString);
		nova_figura.conjunto_vertices[i].x = vertice_x;
		nova_figura.conjunto_vertices[i].y = vertice_y;
		nova_figura.conjunto_vertices[i].z = vertice_z;

		i++;
		n_vertices--;
	}

	//copiar o vector conjuntoT para o vector de transformacoes da nova_figura
	std::copy(conjunto_t.begin(), conjunto_t.end(), std::back_inserter(nova_figura.transformacoes));
	//copiar o conjunto de animacoes para a nova_figura
	std::copy(conjunto_a.begin(), conjunto_a.end(), std::back_inserter(nova_figura.animacoes));

	//inserir no grupo a figura
	
	(*g).conjunto_figuras.push_back(nova_figura);
	

}





void processNormalKeys(unsigned char c, int xx, int yy) {

	// put code to process regular keys in here

}


void processSpecialKeys(int key, int xx, int yy) {

	switch (key) {

	case GLUT_KEY_RIGHT:
		alfa -= 0.4; break;

	case GLUT_KEY_LEFT:
		alfa += 0.4; break;

	case GLUT_KEY_UP:
		beta += 0.4f;
		if (beta > 2.5f)
			beta = 2.5f;
		break;

	case GLUT_KEY_DOWN:
		beta -= 0.4f;
		if (beta < -2.5f)
			beta = -2.5f;
		break;

	case GLUT_KEY_PAGE_DOWN: radius -= 0.8f;
		if (radius < 0.4f)
			radius = 0.4f;
		break;

	case GLUT_KEY_PAGE_UP: radius += 0.9f; break;
	}
	spherical2Cartesian();
	//glutPostRedisplay();

}

void processMouseButtons(int button, int state, int xx, int yy)
{
	if (state == GLUT_DOWN) {
		startX = xx;
		startY = yy;
		if (button == GLUT_LEFT_BUTTON)
			tracking = 1;
		else if (button == GLUT_RIGHT_BUTTON)
			tracking = 2;
		else
			tracking = 0;
	}
	else if (state == GLUT_UP) {
		if (tracking == 1) {
			alfa += (xx - startX);
			beta += (yy - startY);
		}
		else if (tracking == 2) {

			radius -= yy - startY;
			if (radius < 3)
				radius = 3.0;
		}
		tracking = 0;
	}
}


void processMouseMotion(int xx, int yy)
{
	int deltaX, deltaY;
	int alphaAux, betaAux;
	int rAux;

	if (!tracking)
		return;

	deltaX = xx - startX;
	deltaY = yy - startY;

	if (tracking == 1) {

		alphaAux = alfa + deltaX;
		betaAux = beta + deltaY;

		if (betaAux > 85.0)
			betaAux = 85.0;
		else if (betaAux < -85.0)
			betaAux = -85.0;

		rAux = radius;
	}
	else if (tracking == 2) {

		alphaAux = alfa;
		betaAux = beta;
		rAux = radius - deltaY;
		if (rAux < 3)
			rAux = 3;
	}
	camX = rAux * sin(alphaAux * 3.14 / 180.0) * cos(betaAux * 3.14 / 180.0);
	camZ = rAux * cos(alphaAux * 3.14 / 180.0) * cos(betaAux * 3.14 / 180.0);
	camY = rAux * sin(betaAux * 3.14 / 180.0);
}

void ler_XML(std::string ficheiro) {

	std::string nomeFigura;
	figura ftemp;

	XMLDocument doc;
	const char *c = ficheiro.c_str();

	doc.LoadFile(c);

	
	XMLNode* cena = doc.FirstChild();
	//XMLElement* cena = doc.FirstChildElement();

	if (cena == NULL)
	{
		std::cerr << "Documento n�o possui cena definida" << std::endl;
		doc.Clear();
		return;
	}

	//auxiliares para guardar as figuras, transformacoes e grupos na cena
	Grupo novo_grupo;
	Grupo* apt_novo_grupo=&novo_grupo;
	std::vector<transformacao> transformacoes;
	std::vector<animacao> animacoes;
	figura nova_figura;
	nova_figura.conjunto_vertices = NULL;
	nova_figura.transformacoes.clear();
	nova_figura.animacoes.clear();


	for (XMLElement* grupos = cena->FirstChildElement(); grupos != NULL; grupos = grupos->NextSiblingElement()) {

		for (XMLElement* grupo = grupos->FirstChildElement(); grupo != NULL; grupo = grupo->NextSiblingElement()) {
			

			std::string  elementosG = grupo->Value();
			const char * nomeElemento = elementosG.c_str();

			//quando um novo modelo for encontrado vai aplicar a funcao de leitura de vertices e as transformacoes lidas ate ao momento sao
			//guardadas na nova figura e inseridas no grupo
			if (elementosG == "modelo") {
				const XMLAttribute* nome_ficheiro;
				nome_ficheiro = grupo->FirstAttribute();
				ler_figura(nome_ficheiro->Value(),nova_figura,transformacoes,animacoes,apt_novo_grupo);

				//limpar a figura para reutiliza��o
				nova_figura.conjunto_vertices = NULL;
				//limpar vector de transformacoes porque ja foram aplicadas a figura e limpar transformacoes da figura,nao garante realoca��o
				nova_figura.transformacoes.clear();
				nova_figura.animacoes.clear();
				transformacoes.clear();
				animacoes.clear();
		

			}
			//sempre que encontrar uma transformacao, insere no vector temporario transformacoes
			else if (elementosG == "scale") {
				transformacao escala;
				float x, y, z;
				x = 0;
				y = 0;
				z = 0;

				const XMLAttribute* atributosT = grupo->FirstAttribute();
				while (atributosT != NULL) {
					if (strcmp(atributosT->Name(), "x") == 0) {
						x = atributosT->FloatValue();
					}
					if (strcmp(atributosT->Name(), "y") == 0) {
						y = atributosT->FloatValue();
					}
					if (strcmp(atributosT->Name(), "z") == 0) {
						z = atributosT->FloatValue();
					}
					
					atributosT = atributosT->Next();
					
				}
				escala.tipo = 'e';
				escala.x = x;
				escala.y = y;
				escala.z = z;
				transformacoes.push_back(escala);

			}

			else if (elementosG == "rotate") {
				transformacao rotacao;
				float x, y, z, angulo;
				x = 0;
				y = 0;
				z = 0;
				angulo = 0;
				const XMLAttribute* atributosT = grupo->FirstAttribute();
				while (atributosT != NULL) {
					if (strcmp(atributosT->Name(), "angle") == 0) {
						angulo = atributosT->FloatValue();
					}
					if (strcmp(atributosT->Name(), "x") == 0) {
						x = atributosT->FloatValue();
					}
					if (strcmp(atributosT->Name(), "y") == 0) {
						y = atributosT->FloatValue();
					}
					if (strcmp(atributosT->Name(), "z") == 0) {
						z = atributosT->FloatValue();
					}
					atributosT = atributosT->Next();
				}
				rotacao.tipo = 'r';
				rotacao.angulo = angulo;
				rotacao.x = x;
				rotacao.y = y;
				rotacao.z = z;
				transformacoes.push_back(rotacao);

			}
			else if (elementosG == "translate") {
				std::ofstream testetransl;
				testetransl.open("testetransl.txt");
				transformacao translacao;
				float x, y, z,time=0;
				x = 0;
				y = 0;
				z = 0;
				const XMLAttribute* atributosT = grupo->FirstAttribute();
				if (strcmp(atributosT->Name(), "time") == 0) {
					time = atributosT->FloatValue();
					animacao novaanimacao;
					novaanimacao.tempo_animacao = time;
					novaanimacao.tipo_animacao = 't';
					std::vector<verticeANIM> vertices_lidos;
					int contadorv_animacao = 0;
					for (XMLElement* pontos = grupo->FirstChildElement(); pontos != NULL; pontos = pontos->NextSiblingElement()) {
						const XMLAttribute * pontosT = pontos->FirstAttribute();
						while (pontosT != NULL) {
							if (strcmp(pontosT->Name(), "X") == 0) {
								x = pontosT->FloatValue();
							}
							if (strcmp(pontosT->Name(), "Y") == 0) {
								y = pontosT->FloatValue();
							}
							if (strcmp(pontosT->Name(), "Z") == 0) {
								z = pontosT->FloatValue();
							}
							pontosT = pontosT->Next();
						}
						verticeANIM novo_vertice_anim;
						novo_vertice_anim.x = x;
						novo_vertice_anim.y = y;
						novo_vertice_anim.z = z;
						vertices_lidos.push_back(novo_vertice_anim);
						
					}
					novaanimacao.pontos_animacao = pontos_catmull(vertices_lidos, 4);
					animacoes.push_back(novaanimacao);

				}
				else {
					while (atributosT != NULL) {
						if (strcmp(atributosT->Name(), "X") == 0) {
							x = atributosT->FloatValue();
						}
						if (strcmp(atributosT->Name(), "Y") == 0) {
							y = atributosT->FloatValue();
						}
						if (strcmp(atributosT->Name(), "Z") == 0) {
							z = atributosT->FloatValue();
						}

						atributosT = atributosT->Next();
					}
					translacao.tipo = 't';
					translacao.x = x;
					translacao.y = y;
					translacao.z = z;
					translacao.time = time;
					transformacoes.push_back(translacao);

				}
				

			}


		}
		//insere o grupo na cena
		conjunto_cenas.grupos.push_back(*apt_novo_grupo);
		
		//limpa o grupo para reutilizacao
		novo_grupo.conjunto_figuras.clear();

	}

}


int main(int argc, char **argv) {

// put GLUT init here
	
	ler_XML("SS.xml");
	load_catmull("catmull.txt");

	glutInit(&argc, argv);
	glutInitDisplayMode(GLUT_DEPTH|GLUT_DOUBLE|GLUT_RGBA);
	glutInitWindowPosition(100, 100);
	glutInitWindowSize(1200, 920);
	glutCreateWindow("CG_TP_FASE_1");



// put callback registration here
	glutDisplayFunc(renderScene);
	glutIdleFunc(renderScene);
	glutReshapeFunc(changeSize);
	glutTimerFunc(0, timer, 0);
	//glutTimerFunc(1000, timer2, 0);
	//glutTimerFunc(3000, timer3, 0);

	//keyboard
	glutKeyboardFunc(processNormalKeys);
	glutSpecialFunc(processSpecialKeys);

	// mouse
	glutMouseFunc(processMouseButtons);
	glutMotionFunc(processMouseMotion);
	//mouse
// OpenGL settings 
	glEnable(GL_DEPTH_TEST);
	//glEnable(GL_CULL_FACE);
	glClearColor(0.0f,0.0f,0.0f,0.0f);
	glColor3f(0.0, 0.0, 0.0);   //this one
	glPointSize(1);             //and this one

// enter GLUT's main loop
	glutMainLoop();
	
	return 1;
}
int im = 0,jm =0, km=0;
void timer(int a) {
	if (im == conjunto_cenas.grupos.at(0).conjunto_figuras.at(0).animacoes.at(0).pontos_animacao.size()) im = 0;
	glutPostRedisplay();
	glutTimerFunc(10000 / 120, timer, 0);
	e1_x = conjunto_cenas.grupos.at(0).conjunto_figuras.at(0).animacoes.at(0).pontos_animacao.at(im).x;
	e1_y = conjunto_cenas.grupos.at(0).conjunto_figuras.at(0).animacoes.at(0).pontos_animacao.at(im).y;
	e1_z = conjunto_cenas.grupos.at(0).conjunto_figuras.at(0).animacoes.at(0).pontos_animacao.at(im).z;
	im++;

	

}
void timer2(int a) {
	
	if (jm == catmull.n_vertices) jm = 0;
	glutPostRedisplay();
	glutTimerFunc(10000 / 120, timer2, 0);
	e2_x = catmull.conjunto_vertices[jm].x * 3;
	e2_y = catmull.conjunto_vertices[jm].y;
	e2_z = catmull.conjunto_vertices[jm].z * 3;

	jm++;


}
void timer3(int a) {
	if (km == catmull.n_vertices) km = 0;
	glutPostRedisplay();
	glutTimerFunc(10000 / 120, timer3, 0);
	e3_x = catmull.conjunto_vertices[km].x * 6;
	e3_y = catmull.conjunto_vertices[km].y;
	e3_z = catmull.conjunto_vertices[km].z * 6;
	km++;


}




